
class Pantry
  attr_reader :shopping_list, :stock
  def initialize
    @stock = Hash.new(0)
    @shopping_list = Hash.new(0)
    @cookbook = []
    @temp_stock = nil
  end

  def stock_check(item)
    @stock[item]
  end

  def restock(item, count)
    @stock[item] = count
  end

  def add_to_shopping_list(recipe)
    recipe.ingredients.each do |key, value|
      @shopping_list[key] += (@stock[key] - value).abs
    end
  end

  def print_shopping_list
    string = ""
    @shopping_list.each do |key, value|
      string << "* #{key}: #{value}\n"
    end
    string.chomp
  end

  def add_to_cookbook(recipe)
    @cookbook << recipe
  end

  def meal_checker
    @cookbook.find_all do |recipe|
      recipe.ingredients.all? { |key, value| @stock[key] >= value }
    end
  end

  def what_can_i_make
    meals = meal_checker
    meals = meals.map {|value| value.name}
  end
  
  def how_many_can_i_make
    meals = meal_checker
    @temp_Stock = @stock
    until meals.nil?
    meals.each do |recipe|
      recipe.ingredients do |key, value|
        temp_stock[key] - value

end
